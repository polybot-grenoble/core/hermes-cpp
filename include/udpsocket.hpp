#pragma once

#ifdef HERMES_CPP_V1

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <arpa/inet.h>

#include <unistd.h>
#include <stdint.h>

#include <map>
#include <string>
#include <string.h>
#include <vector>

#include "socket.hpp"

namespace Core::Sockets {

    constexpr uint32_t CORE_MAX_UDP_LEN = 1024;

    /**
     * @brief Object representing a UDP Client address
     */
    typedef struct {
        struct in_addr addr;
        uint16_t port;
    } UDPClientAddr;

    /**
     * @brief UDP (IPv4) adapter for Core. 
     */
    class UDPSocket {

        protected:
        int socketID;
        bool opened = false;
        UDPClientAddr self;

        public:

        /**
         * @brief Port to listen to 
         */
        uint16_t port;

        /**
         * @brief Construct a new SocketCAN object
         */
        UDPSocket ();

        /**
         * @brief Destroy the SocketCAN object
         */
        ~UDPSocket();

        /**
         * @brief Opens a socket
         * 
         * @param port The port to open
         * @return An error code if failed.
         */
        SocketReturnCode open (uint16_t port = 42069);

        /**
         * @brief Closes the socket
         */
        void end ();

        /**
         * @brief Writes data to the interface
         * 
         * @param id The address of the client
         * @param data The data of the frame
         * @param len The length of data in the frame
         * @return An error code if failed.
         */
        SocketReturnCode writeData (
            UDPClientAddr addr, 
            std::vector<uint8_t> &data
        );

        /**
         * @brief Reads data from the bus 
         * 
         * @param addr The address of the client
         * @param data The data of the frame
         * @param len The length of data in the frame
         * @return SocketReturnCode 
         */
        SocketReturnCode readData (
            UDPClientAddr &addr, 
            std::vector<uint8_t> &data
        ); 


    };
    
} 

#endif