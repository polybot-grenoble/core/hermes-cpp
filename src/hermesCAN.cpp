#include "hermesCAN.hpp"

#ifdef HERMES_CPP_V1

namespace Core::Sockets {

    HermesCAN::HermesCAN (): SocketCAN() {
        Hermes_init(&hermes, 1, 8);
    }

    HermesCAN::HermesCAN (int id): SocketCAN() {
        Hermes_init(&hermes, id, 8);
    }

    HermesCAN::~HermesCAN () {
        if (running) {
            stop();
        }
    }

    bool HermesCAN::start () {

        if (!opened) return false;

        // Put the running flag to true
        running = true;

        // Starting the reading thread
        // To pass a member function to a thread, you must also pass an instance 
        // on which to execute that function.
        // https://stackoverflow.com/a/31322004
        reader = std::thread(&HermesCAN::readingThread, this);
        writer = std::thread(&HermesCAN::writingThread, this);

        return true;

    }

    void HermesCAN::stop () {

        // Stopping threads
        running = false;

        // Waiting for Reading thread to stop
        pthread_cancel(reader.native_handle());
        if (reader.joinable()) {
            reader.join();
        }

        // Waiting for Writer thread to stop
        if (writer.joinable()) {
            flush();    // Makes sure to unlock the writer
            writer.join();
        }

    }


    SocketReturnCode HermesCAN::getLastError () {
        return lastError;
    }

    bool HermesCAN::isRunning () {
        return running;
    }


    bool HermesCAN::getBuffer (HermesBuffer *buffer) {

        HermesBuffer *temp = Hermes_nextBuffer(&hermes);

        // We retrieved all available buffers
        if (!temp) return false;

        // We copy the buffer
        memcpy(buffer, temp, sizeof(HermesBuffer));

        // We clear the internal one
        Hermes_clearBuffer(temp);

        return true;

    }

    bool HermesCAN::sendBuffer (HermesBuffer *buffer) {

            HermesBuffer *out;

            // Cannot send if not opened
            if (!opened) return false;
            
            // Getting an empty send buffer
            HermesReturnCode code = Hermes_getEmptyOutputBuffer(
                &hermes, &out
            );

            // We have to wait for a next time to send our buffer
            if (code) return false;

            // Copying the buffer
            memcpy(out, buffer, sizeof(HermesBuffer));

            semaphore.acquire();

            // Sending the buffer
            int index = Hermes_getOutputBufferIndex(&hermes, out);
            Hermes_sendBuffer(&hermes, index);

            semaphore.release();         
            flush();

            return true;

    }

    void HermesCAN::flush() {

        pendingWrites.release();

    }

    void HermesCAN::readingThread () {

        uint32_t can_id,    // ID of the CAN Frame
                 len;       // Length of the data

        // Data buffer   
        uint8_t *data = (uint8_t *) malloc(sizeof(uint8_t) * hermes.chunkLength);

        // return code of the reading function
        SocketReturnCode code;        

        // Data from the id
        uint8_t reciever;
        uint16_t command;
        uint8_t sender;
        bool isResponse;

        // Main thread loop
        while (running)
        {
            // We try to read the next available thing on the bus
            code = readData(&can_id, data, &len);
            if (code) {
                // The read failed, we quit reading
                lastError = code;
                running = false;
                break;
            }

            // Parsing the ID
            Hermes_parseCANFrameID(
                can_id, &reciever, &command, &sender, &isResponse
            );

            // If the message is not for us, we ignore it
            if (reciever != hermes.id) {
                continue;
            }

            // Waiting for Hermes to be available
            semaphore.acquire();

            // Giving Hermes the data
            Hermes_recieveData(&hermes, sender, command, data, len, isResponse);

            // Releasing Hermes
            semaphore.release();
        }
        
        // Freeing data buffer
        free(data);

    }

    void HermesCAN::writingThread () {

        HermesReturnCode code;
        uint8_t remote;
        uint16_t command;
        uint32_t isResponse;
        size_t payloadSize;
        uint8_t payload[8];

        int id;

        // We send until we're done
        while (running) {

            // We wait for a buffer
            pendingWrites.acquire();
            code = HERMES_OK;

            semaphore.acquire();

            while (!code) {
                
                // We retrieve the next thing to send
                code = Hermes_sendData(
                    &hermes, &remote, &command, &isResponse, &payloadSize, payload
                );                

                if (code) {
                    // We don't have anything to send
                    continue;
                }

                // We create the CAN frame ID
                id = Hermes_serializeCANFrameID(
                    remote, command, hermes.id, isResponse
                );

                // We write it to the bus
                writeData(id, payload, payloadSize);

            }

            semaphore.release();

        }

    }

}

#endif