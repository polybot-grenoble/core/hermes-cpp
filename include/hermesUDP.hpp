#pragma once
#ifdef HERMES_CPP_V1

#include <thread>
#include <chrono>
#include <semaphore>
#include <map>
#include <vector>

extern "C" {
    #include "Hermes.h"
}
#include "udpsocket.hpp"

namespace Core::Sockets {

    /**
     * Flow-control codes for communication over UDP
     */
    typedef enum {
        // A hermes request buffer is sent
        HERMES_REQ,
        // A hermes response buffer is sent
        HERMES_RES,
        // Someone wants to know who's on the network
        HERMES_CLIENT_DISCOVER,
        // Someone tells us he exists
        HERMES_CLIENT_ANNOUNCE,
    } HermesUDPOPCode;

    /**
     * Size of the Header produced for UDP
     */
    constexpr uint32_t HERMES_UDP_HEADER_SIZE = 
        sizeof(HermesUDPOPCode) + sizeof(uint16_t);

    /**
     * Maximum size of a hermes chunk
     */
    constexpr uint32_t HERMES_MAX_UDP_DATA = 
        CORE_MAX_UDP_LEN - (sizeof(HermesUDPOPCode) + sizeof(uint16_t));

    /**
     * @brief A wrapper C++ class for Hermes using UDP
     */
    class HermesUDP : public UDPSocket {

        private:

            /** The last Socket return code */
            SocketReturnCode lastError = SOCK_OK;

            /** Is Hermes running */
            bool running;

            /** 
             * Internal semaphore for thread-safe operations, initialized to 1 
             */
            std::binary_semaphore semaphore{1}; 

            std::counting_semaphore<HERMES_BUFFER_COUNT> pendingWrites{0};

            /** Internal reading thread */
            std::thread reader;

            /** Internal writer thread */
            std::thread writer;

            /** Internal maps to find the clients */
            std::map<uint8_t, UDPClientAddr> hermesIDtoUDP;

        public:
            /** The C hermes struct */
            Hermes_t hermes;

            /**
             * @brief Construct a new HermesUDP object with ID 1
             */
            HermesUDP ();

            /**
             * @brief Construct a new HermesUDP object
             * 
             * @param id The ID of this device
             */
            HermesUDP (int id);

            /**
             * @brief Destroy the HermesUDP object
             */
            ~HermesUDP ();

            /**
             * @brief Starts the HermesUDP Thread
             * 
             * @return true Hermes started
             * @return false SocketCAN is not opened
             */
            bool start ();

            /**
             * @brief Stops Hermes. Blocking until thread join
             */
            void stop ();

            /**
             * @brief Returns the last available SocketCAN error code
             * 
             * @return SocketReturnCode 
             */
            SocketReturnCode getLastError ();

            /**
             * @brief Indicates if Hermes is running 
             */
            bool isRunning ();

            /**
             * @brief Get the next recieved buffer by copying the one recieved
             * 
             * @param buffer The buffer object to copy to
             * @return true A buffer has been copied,
             * @return false No buffer copied
             */
            bool getBuffer (HermesBuffer *buffer);

            /**
             * @brief Sends a buffer on the CAN bus
             * 
             * @param buffer The buffer to send
             * @return true The buffer has been sent,
             * @return false The buffer has been lost
             */
            bool sendBuffer (HermesBuffer *buffer);

            /**
             * @brief Forces hermes to send all pending buffers left in its 
             * queue 
             */
            void flush ();

            /**
             * @brief Get the Client HermesID associated to its address. 
             * Threadsafe.
             * 
             * @param addr The address to look up
             * @return uint8_t The ID, 255 as a fallback value (i.e. broadcast)
             */
            uint8_t getClientHermesID (UDPClientAddr addr);

            /**
             * @brief Get the Client address associated to its hermesID.
             * Threadsafe.
             * 
             * @param hermesID the ID to look up
             * @return uint8_t The address, addr=port=0 when non-existent
             */
            UDPClientAddr getClientAddr (uint8_t hermesID);

            /**
             * @brief Associates a client Hermes ID with its address. 
             * Threadsafe.
             * 
             * @param hermesID The hermes ID of the peripheral
             * @param addr The address of the peripheral
             */
            void setClient (uint8_t hermesID, UDPClientAddr addr);

            /**
             * @brief Associates a client Hermes ID with its address. 
             * Threadsafe.
             * 
             * @param hermesID The hermes ID of the peripheral
             * @param ipv4 The host of the peripheral
             * @param port The port of the peripheral
             */
            void setClient (uint8_t hermesID, std::string ipv4, uint16_t port);

            /**
             * @brief Reading thread for Hermes 
             */
            void readingThread ();

            /**
             * @brief Writer thread for Hermes
             */
            void writingThread ();

    };

    /**
     * @brief Serializes data to be sent over the UDP socket
     * 
     * @param opCode The flow-control code
     * @param command The command to send
     * @param data The data to send
     * @return The buffer to send
     */
    std::vector<uint8_t> Hermes_serializeUDP (
        HermesUDPOPCode opCode,
        uint16_t command,
        std::vector<uint8_t> data
    );

    /**
     * @brief Parses data from a recieved UDP buffer
     * 
     * @param buffer The buffer to parse
     * @param opCode The flow-control code contained in the buffer 
     * @param command The command recieved
     * @param data The vector in which we store data
     */
    void Hermes_parseUDP (
        std::vector<uint8_t> buffer,
        HermesUDPOPCode *opCode,
        uint16_t *command,
        std::vector<uint8_t> &data
    ); 
    

}

#endif