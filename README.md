# Hermes CPP wrappers

Needs [Hermes](https://gitlab.com/polybot-grenoble/core/hermes) to work.

Every class is documented using javadoc-like comments.

## SocketCAN

This module wraps SocketCAN to implement HermesCAN.

## UDP

A wrapper has been written to allow the creation of HermesUDP without re-writing
all of the code from scratch.

## HermesCAN and HermesUDP

Both of these classes combines Hermes with it's communication layer. 

HermesCAN/UDP rely on a reader and writer thread, that need to be started after
setting the CAN interface or UDP port to listen/write to. 

The threads can be stopped manually or are (in theory) stopped when the class
reaches it's destructor.

## CMake

Have fun with CMake, it's an ever-lasting fight to have your code compiled 
in the end.

Hermes-CPP will try to find Hermes in the following locations in your project 
tree:

- `/hermes`
- `/lib/hermes`

