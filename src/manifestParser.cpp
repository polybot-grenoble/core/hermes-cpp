#include "manifestParser.hpp"
#include <string.h>

using namespace Hermes;

// --- Argument -----------------------------------------------------------

PeripheralCommandArgument::PeripheralCommandArgument() {
	// Nothing to do here
}

PeripheralCommandArgument::PeripheralCommandArgument(const uint8_t* mem):
	typePtr(mem), namePtr(mem + sizeof(uint8_t)) {

};

PeripheralCommandArgument::PeripheralCommandArgument(
	const PeripheralCommandArgument& arg
) : typePtr(arg.typePtr), namePtr(arg.namePtr) {

}

HermesArgumentTypeIndicator PeripheralCommandArgument::type() const {
	if (!typePtr) {
		return HERMES_T_NONE;
	}

	auto ptr = reinterpret_cast<const uint8_t*>(typePtr);
	auto type = static_cast<HermesArgumentTypeIndicator>(*ptr);
	return type;
}

std::string PeripheralCommandArgument::name() const {
	if (!namePtr) {
		return "";
	}

	auto ptr = reinterpret_cast<const char*>(namePtr);
	return std::string(ptr); // Hope there is a null char at the end
}

size_t PeripheralCommandArgument::memLength() const {
	if (!typePtr || !namePtr) {
		return 0;
	}

	auto str = reinterpret_cast<const char*>(namePtr);
	return sizeof(uint8_t) + strlen(str) + 1;
}

// --- Command -----------------------------------------------------------

PeripheralCommand::PeripheralCommand():
	mem(nullptr), idPtr(nullptr), namePtr(nullptr), descriptionPtr(nullptr) {

}

PeripheralCommand::PeripheralCommand(const uint8_t* mem):
	mem(mem), 
	idPtr(mem + sizeof(uint16_t)), 
	namePtr(mem + sizeof(uint16_t) + sizeof(uint16_t)),
	descriptionPtr(
		(mem + sizeof(uint16_t) + sizeof(uint16_t))
		+ strlen(
			reinterpret_cast<const char*>(
				mem + sizeof(uint16_t) + sizeof(uint16_t)
			)
		) + 1U	
	) {

	// Parse arguments
	auto ptr = reinterpret_cast<const uint8_t*>(descriptionPtr);
	ptr += strlen(reinterpret_cast<const char*>(ptr)) + 1U;

	uint16_t argCount, i;

	// Get input arg count
	memcpy(&argCount, ptr, sizeof(uint16_t));
	ptr += sizeof(uint16_t);

	// Parse input args
	inputs.resize(argCount);
	for (i = 0; i < argCount; i++) {
		PeripheralCommandArgument arg (ptr);
		inputs[i] = arg;
		ptr += arg.memLength();
	}

	// Get output arg count
	memcpy(&argCount, ptr, sizeof(uint16_t));
	ptr += sizeof(uint16_t);

	// Parse output args
	outputs.resize(argCount);
	for (i = 0; i < argCount; i++) {
		PeripheralCommandArgument arg(ptr);
		outputs[i] = arg;
		ptr += arg.memLength();
	}

}

PeripheralCommand::PeripheralCommand(const PeripheralCommand& cmd) :
	mem(cmd.mem),
	idPtr(cmd.idPtr),
	namePtr(cmd.namePtr),
	descriptionPtr(cmd.descriptionPtr),
	inputs(cmd.inputs),
	outputs(cmd.outputs) {
	// The other class did all the work for us
}

uint16_t PeripheralCommand::id() const {
	if (!idPtr) {
		return 4096;
	}
	return (*reinterpret_cast<const uint16_t*>(idPtr)) & 0x0fff; // 12-bit value
}

std::string PeripheralCommand::name() const {
	if (!namePtr) {
		return "";
	}

	auto ptr = reinterpret_cast<const char*>(namePtr);
	return std::string(ptr); // Hope there is a null char at the end
}

std::string PeripheralCommand::description() const {
	if (!descriptionPtr) {
		return "";
	}

	auto ptr = reinterpret_cast<const char*>(descriptionPtr);
	return std::string(ptr); // Hope there is a null char at the end
}

bool PeripheralCommand::responds() const {
	if (!idPtr) {
		return false;
	}
	return ((*reinterpret_cast<const uint16_t*>(idPtr)) >> 12) & 0x1;
}

size_t PeripheralCommand::memLength() const {

	auto ptr = reinterpret_cast<const uint8_t*>(descriptionPtr);
	ptr += strlen(reinterpret_cast<const char*>(ptr)) + 1U;

	size_t cmdSize = ptr - reinterpret_cast<const uint8_t*>(mem);

	for (auto& arg : inputs) {
		cmdSize += arg.memLength();
	}

	for (auto& arg : outputs) {
		cmdSize += arg.memLength();
	}

	return cmdSize;

}

PeripheralManifest::PeripheralManifest() {

	memPtrLength =
		sizeof(uint32_t) +
		sizeof(uint8_t) +
		16 * sizeof(char) +
		sizeof(uint16_t) +
		sizeof(uint8_t);

	memPtr = (void*)calloc(memPtrLength, sizeof(uint8_t));

}

PeripheralManifest::PeripheralManifest(const void* mem) {

	uint32_t size;
	memcpy(&size, mem, sizeof(uint32_t));

	memPtrLength = size;
	memPtr = (void*)malloc(size);
	memcpy(memPtr, mem, memPtrLength);

	idPtr = reinterpret_cast<uint8_t*>(memPtr) + sizeof(uint32_t);
	ipPtr = reinterpret_cast<uint8_t*>(idPtr) + sizeof(uint8_t);
	portPtr = reinterpret_cast<uint8_t*>(ipPtr) + sizeof(uint8_t) * 16;
	familyPtr = reinterpret_cast<uint8_t*>(portPtr) + sizeof(uint16_t);

	auto ptr = reinterpret_cast<uint8_t*>(familyPtr)
		+ strlen(reinterpret_cast<const char*>(familyPtr)) + 1U;

	uint16_t cmdLen;
	for (
		memcpy(&cmdLen, ptr, sizeof(uint16_t)); 
		cmdLen; 
		memcpy(&cmdLen, (ptr += cmdLen + sizeof(uint16_t)), sizeof(uint16_t))
	) {
		PeripheralCommand cmd(ptr);
		commands.push_back(cmd);
	}

}

PeripheralManifest::PeripheralManifest(const PeripheralManifest& manifest):
	PeripheralManifest(manifest.memPtr) {
	// Copy again
}

PeripheralManifest::~PeripheralManifest() {
	free(memPtr);
}


uint8_t PeripheralManifest::id() const {
	return *reinterpret_cast<const uint8_t*>(idPtr);
}

std::string PeripheralManifest::ipv4() const {
	auto str = reinterpret_cast<const char*>(ipPtr);
	return std::string(str);
}

uint16_t PeripheralManifest::port() const {
	return *reinterpret_cast<const uint8_t*>(portPtr);
}

std::string PeripheralManifest::family() const {
	auto str = reinterpret_cast<const char*>(familyPtr);
	return std::string(str);
}

size_t PeripheralManifest::memLength() const {
	return memPtrLength;
}


std::ostream& Hermes::operator<<(std::ostream& out, const PeripheralCommandArgument& arg) {
	return out << arg.name() << ": " << static_cast<char>(arg.type() & 0x7f);
}

std::ostream& Hermes::operator<<(std::ostream& out, const PeripheralCommand& cmd) {

	out << cmd.name() << "[" << cmd.id() << "]";

	out << "(";
	if (cmd.inputs.size()) {
		for (
			auto arg = cmd.inputs.begin(); 
			arg != cmd.inputs.end(); 
			arg++
		) {
			if (arg != cmd.inputs.begin()) {
				out << ", ";
			}
			out << (*arg);
		}
	}
	out << ")";

	if (cmd.responds()) {
		out << " -> (";
		if (cmd.outputs.size()) {
			for (
				auto arg = cmd.outputs.begin();
				arg != cmd.outputs.end();
				arg++
				) {
				if (arg != cmd.outputs.begin()) {
					out << ", ";
				}
				out << (*arg);
			}
		}
		out << ")";
	}

	out << "; // " << cmd.description();

	return out;
}

std::ostream& Hermes::operator<<(std::ostream& out, const PeripheralManifest& manifest) {

	out << "Peripheral " << manifest.family() << ":" << static_cast<uint32_t>(manifest.id());

	if (manifest.port()) {
		out << " @ " << manifest.ipv4() << ":" << manifest.port();
	}

	if (manifest.commands.size()) {
		for (auto& cmd : manifest.commands) {
			out << "\r\n ." << cmd;
		}
	}

	return out << "\r\n";
}

