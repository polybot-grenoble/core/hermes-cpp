#pragma once
extern "C" {
    #include <Constants.h>
    #include <Hermes.h>
    #include <base64.h>
}
#include <vector>
#include <string>
#include <stdexcept>

/**
 * Wrapper for the HermesBuffer C object 
 */

namespace Hermes {

class Buffer {

    struct Argument {
        /**
         * @brief Argument Position in buffered data
         */
        size_t pos = 0;

        /**
         * @brief Argument type 
         */
        HermesArgumentTypeIndicator type = HERMES_T_NONE;

        /** Possible output types */
        union Value {
            int8_t i8; int16_t i16; int32_t i32; int64_t i64;
            char c; float f; double d; 
            const char *str; const uint8_t *blob;
        };

    };

private:
    /** Internal buffer */
    HermesBuffer buffer;

    /** Buffer command arguments */
    std::vector<Argument> arguments;
    
    /**
     * @brief Parses the arguments that are present in the buffer
     */
    void parseBufferArguments ();

    /**
     * @brief Peripheral that sends/has sent this buffer.
     * May be added to the C Buffer definition ?
     */
    uint8_t sender;

public:

    /** Initializes an empty Hermes Buffer */
    Buffer (void);

    /**
     * @brief Construct a new Buffer object by copying an existing one
     * 
     * @param source The source buffer to copy from
     */
    Buffer (HermesBuffer* source);

    /**
     * @brief Initializes an empty Hermes Buffer with 
     * the desired header
     * 
     * @param command Buffer command number
     * @param remote Remote peripheral ID
     * @param isResponse Request/Response flag
     * @param signature Buffer argument signature
     * @param sender Sending peripheral ID
     */
    Buffer(
        uint16_t command, 
        uint8_t remote, 
        bool isResponse = false, 
        const char *signature = "",
        uint8_t sender = 0
    );

    /**
     * @brief Construct a new Buffer object by copying an existing one
     * 
     * @param source The source buffer to copy from
     */
    Buffer (Buffer& source);

    /**
     * @brief Construct a new Buffer from a base64-encoded buffer
     * @param base64 Base64-encoded HermesBuffer
     */
    Buffer(std::string& base64);

    /**
     * @brief Destroy the Buffer object 
     */
    ~Buffer (void);

    /// Arguments handling

    /**
     * @brief Get the Buffer Argument Signature
     * @return std::string Buffer Argument Signature representation
     */
    std::string getSignature ();

    /**
     * @brief Set the Buffer Argument Signature
     * @param signature String Buffer Argument Signature representation
     */
    void setSignature (std::string signature);

    /**
     * @brief Adds an argument slot in buffered data     
     * @param type Type of the argument slot
     */
    void addArgument (HermesArgumentTypeIndicator type);

    /**
     * @brief Gets the value of the n-th argument
     * 
     * @param n Index of the argument
     * @return Argument::Value Value of the argument
     */
    Argument::Value get (size_t n);

    /**
     * @brief Get the type of the n-th argument
     * 
     * @param n Argument index
     * @return Argument type
     */
    HermesArgumentTypeIndicator getType (size_t n);

    /**
     * @brief Get the string from the n-th argument
     * @param n Argument index
     * @return String
     */
    std::string getString(size_t n);

    /**
     * @brief Get the blob from the n-th argument
     * @param n Argument index
     * @return Blob
     */
    std::vector<uint8_t> getBlob(size_t n);

    /**
     * @brief Sets the n-th argument
     */
    void set(size_t n, char c);
    /**
     * @brief Sets the n-th argument
     */
    void set(size_t n, int8_t i8);
    /**
     * @brief Sets the n-th argument
     */
    void set(size_t n, int16_t i16);
    /**
     * @brief Sets the n-th argument
     */
    void set(size_t n, int32_t i32);
    /**
     * @brief Sets the n-th argument
     */
    void set(size_t n, int64_t i64);
    /**
     * @brief Sets the n-th argument
     */
    void set(size_t n, float f);
    /**
     * @brief Sets the n-th argument
     */
    void set(size_t n, double d);

    /**
     * @brief Sets the n-th argument. Tries to move the next arguments
     * to fit the contents, else fails.
     */
    void set(size_t n, const char* str);
    /**
     * @brief Sets the n-th argument. Tries to move the next arguments
     * to fit the contents, else fails.
     */
    void set(size_t n, std::string str);

    /**
     * @brief Sets the n-th argument. Changes the length of the buffer 
     * according to the blob length
     */
    void set(size_t n, const uint8_t* blob, size_t size);
    /**
     * @brief Sets the n-th argument. Changes the length of the buffer
     * according to the blob length
     */
    void set(size_t n, std::vector<uint8_t> blob);


    /// RAW BUFFER

    /**
     * @brief Returns the raw internal HermesBuffer
     * @return HermesBuffer& Internal buffer
     */
    HermesBuffer* raw ();

    /// Raw Getters    
    /**
     * @brief Get the Command number
     * @return uint16_t Buffer command number
     */
    uint16_t getCommand ();
    /**
     * @brief Get the Remote peripheral ID
     * @return uint8_t Buffer remote peripheral ID
     */
    uint8_t getRemote ();
    /**
     * @brief Get the Sender peripheral ID
     * @return uint8_t Buffer sender peripheral ID
     */
    uint8_t getSender();
    /**
     * @brief Get the Buffer Data Length
     * @return uint16_t Buffer data length
     */
    uint16_t getLength ();
    /**
     * @brief Get the in-buffer read/write head Position
     * @return uint16_t In-buffer read/write head Position
     */
    uint16_t getPosition ();
    /**
     * @brief Get the buffer Is Response flag
     * 
     * @return true Buffer is a response,
     * @return false Buffer is a request
     */
    bool getIsResponse ();

    /**
     * @brief Get a copy of buffered Data
     * 
     * @return std::vector<uint8_t> Copy of buffered data
     */
    std::vector<uint8_t> getData();

    /// Raw setters
    
    /**
     * @brief Set the Command number
     * @param cmd Command number, < 4096
     */
    void setCommand (uint16_t cmd);

    /**
     * @brief Set the Remote peripheral ID
     * @param remote Remote peripehral ID
     */
    void setRemote (uint8_t remote);

    /**
     * @brief Set the Sending peripheral ID
     * @param sender Sending peripehral ID
     */
    void setSender(uint8_t sender);

    /**
     * @brief Set the Is Response object
     * @param isResponse true for Response, false for Request
     */
    void setIsResponse (bool isResponse);

    /**
     * @brief Set the buffered Data 
     * @param data Must be of length < HERMES_MAX_
     */
    void setData (std::vector<uint8_t> data);

    /// Conversions

    /**
     * @brief Creates a base64 representation of this buffer
     * @return Base64 encoded string
     */
    std::string base64();

    /// Static
    /**
     * @brief Computes the base length of a buffer argument type
     * 
     * @param type Type to get the length from 
     * @return size_t Data Length for this type
     */
    static size_t argumentLength (HermesArgumentTypeIndicator type);

    /**
     * @brief Computes the length of a raw buffer argument
     * 
     * @param argument Memory location storing the raw argument 
     * @param maxLen Maximum length left in buffer data
     * @return size_t Size of the argument
     */
    static size_t argumentLength (uint8_t *argument, uint16_t maxLen);

    /// Cosmetics

    /**
     * @brief Creates a string representation of the buffer contents
     * @return A string representation of the buffer contents
     */
    std::string repr();

    /**
     * @brief Gives a name of a given argument type
     * @param type Argument type
     * @return Name of the type
     */
    static std::string argumentType(HermesArgumentTypeIndicator type);

    /**
     * @brief Returns the value of an argument as a string
     * @param arg Argument index to get the value from
     * @return String representation of the argument value
     */
    std::string argumentToString(uint16_t arg);

};

}