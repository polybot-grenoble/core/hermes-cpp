#pragma once
#ifdef HERMES_CPP_V1

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>
#include <stdint.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include <string>
#include <string.h>

#include "socket.hpp"

namespace Core::Sockets {


    /**
     * @brief SocketCAN adapter for Core. 
     * 
     */
    class SocketCAN {

        protected:
        int socketID;
        bool opened = false;

        public:

        /**
         * @brief Construct a new SocketCAN object
         */
        SocketCAN ();

        /**
         * @brief Destroy the SocketCAN object
         */
        ~SocketCAN();

        /**
         * @brief Opens a CAN socket
         * 
         * @param iface The name of the interface to open
         * @return An error code if failed.
         */
        SocketReturnCode open (char *iface);

        /**
         * @brief Writes data to the CAN interface
         * 
         * @param id The id of the CAN frame
         * @param data The data of the CAN frame
         * @param len The length of data in the CAN frame
         * @return An error code if failed.
         */
        SocketReturnCode writeData (uint32_t id, void *data, uint32_t len);

        /**
         * @brief Reads data from the CAN bus 
         * 
         * @param id The id of the CAN frame
         * @param data The data of the CAN frame
         * @param len The length of data in the CAN frame
         * @return SocketCANReturnCode 
         */
        SocketReturnCode readData (uint32_t *id, void *data, uint32_t *len); 

    };
    
} 

#endif