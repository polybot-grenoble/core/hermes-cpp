#include "hermesUDP.hpp"

#ifdef HERMES_CPP_V1

namespace Core::Sockets {

    HermesUDP::HermesUDP (): UDPSocket() {
        Hermes_init(&hermes, 1, HERMES_MAX_UDP_DATA);
    }

    HermesUDP::HermesUDP (int id): UDPSocket() {
        Hermes_init(&hermes, id, HERMES_MAX_UDP_DATA);
    }

    HermesUDP::~HermesUDP () {
        if (running) {
            stop();
        }
    }

    bool HermesUDP::start () {

        if (!opened) return false;

        // Put the running flag to true
        running = true;

        // Starting the reading thread
        // To pass a member function to a thread, you must also pass an instance 
        // on which to execute that function.
        // https://stackoverflow.com/a/31322004
        reader = std::thread(&HermesUDP::readingThread, this);
        writer = std::thread(&HermesUDP::writingThread, this);

        return true;

    }

    void HermesUDP::stop () {

        // Stopping threads
        running = false;

        // Ending the socket
        end();

        // Waiting for Reading thread to stop
        if (reader.joinable()) {
            reader.join();
        }

        // Waiting for Writer thread to stop
        if (writer.joinable()) {
            flush();
            writer.join();
        }

    }


    SocketReturnCode HermesUDP::getLastError () {
        return lastError;
    }

    bool HermesUDP::isRunning () {
        return running;
    }


    bool HermesUDP::getBuffer (HermesBuffer *buffer) {

        HermesBuffer *temp = Hermes_nextBuffer(&hermes);

        // We retrieved all available buffers
        if (!temp) return false;

        // We copy the buffer
        memcpy(buffer, temp, sizeof(HermesBuffer));

        // We clear the internal one
        Hermes_clearBuffer(temp);

        return true;

    }

    bool HermesUDP::sendBuffer (HermesBuffer *buffer) {

            HermesBuffer *out;
            
            // Cannot send if not opened
            if (!opened) return false;
            
            // Getting an empty send buffer
            HermesReturnCode code = Hermes_getEmptyOutputBuffer(
                &hermes, &out
            );

            // We have to wait for a next time to send our buffer
            if (code) return false;

            // Copying the buffer
            memcpy(out, buffer, sizeof(HermesBuffer));

            semaphore.acquire();

            // Sending the buffer
            int index = Hermes_getOutputBufferIndex(&hermes, out);
            Hermes_sendBuffer(&hermes, index);

            semaphore.release();    
            flush();

            return true;

    }

    void HermesUDP::flush () {

        pendingWrites.release();

    }

    uint8_t HermesUDP::getClientHermesID (UDPClientAddr client) {

        uint8_t client_id = 255;

        semaphore.acquire();

        // Finding the ID
        for (auto [id, addr] : hermesIDtoUDP) {

            if (
                (addr.addr.s_addr == client.addr.s_addr)
                && (addr.port == client.port)
            ) {
                client_id = id;
                break;
            }

        }

        semaphore.release();

        return client_id;

    }

    UDPClientAddr HermesUDP::getClientAddr (uint8_t hermesID) {

        UDPClientAddr addr = { 0, 0 };

        semaphore.acquire();

        // Finding the Address
        auto entry = hermesIDtoUDP.find(hermesID);
        if (entry != hermesIDtoUDP.end()) {
            addr = entry->second;
        }

        semaphore.release();

        return addr;

    }

    void HermesUDP::setClient (uint8_t hermesID, UDPClientAddr addr) {
        // Just a wrapper using the internal semaphore
        semaphore.acquire();

        hermesIDtoUDP[hermesID] = addr;

        semaphore.release();

    }

    void HermesUDP::setClient (
        uint8_t hermesID, std::string ipv4, uint16_t port
    ) {

        UDPClientAddr addr;
        inet_pton(AF_INET, ipv4.c_str(), (struct in_addr*) &addr.addr);
        addr.port = htons(port);

        setClient(hermesID, addr);

    }

    void HermesUDP::readingThread () {

        // return code of the reading function
        SocketReturnCode code;        

        // Buffer
        UDPClientAddr client;
        std::vector<uint8_t> buffer, data;
        HermesUDPOPCode opCode;

        // Data from the buffer
        uint16_t command;
        uint8_t sender;
        bool isResponse;

        // Main thread loop
        while (running)
        {
            // TO CHANGE
            // We try to read the next available thing on the bus
            code = readData(client, buffer);

            if (code) {
                // The read failed, we quit reading
                lastError = code;
                running = false;
                break;
            }

            if (buffer.size() < HERMES_UDP_HEADER_SIZE) {
                // This is garbage, we ignore it
                continue;
            }

            // Parsing the frame
            Hermes_parseUDP(buffer, &opCode, &command, data);
            
            // Acting according to the opCode
            isResponse = opCode == HERMES_RES;
            if (opCode == HERMES_CLIENT_ANNOUNCE) {
                // We add the client to our lookup table
                // The hermes ID is stored in the command (because we can)
                setClient(command, client);

                // We're done
                continue;
            } else if (opCode == HERMES_CLIENT_DISCOVER) {
                // We send a special packet on the UDP socket 
                data.clear();
                buffer = Hermes_serializeUDP(
                    HERMES_CLIENT_ANNOUNCE,
                    hermes.id, 
                    data
                );
                writeData(client, buffer);

                // We're done
                continue;
            }

            // Getting the client's id
            sender = getClientHermesID(client);

            // Waiting for Hermes to be available
            semaphore.acquire();

            // Giving Hermes the data
            Hermes_recieveData(&hermes, sender, command, data.data(), data.size(), isResponse);

            // Releasing Hermes
            semaphore.release();
        }
        
        // Freeing data buffer
        // free(data);

    }

    void HermesUDP::writingThread () {

        HermesReturnCode code;
        uint8_t remote;
        uint16_t command;
        uint32_t isResponse;
        size_t payloadSize;
        uint8_t payload[HERMES_MAX_UDP_DATA];

        std::vector<uint8_t> buffer, data;

        UDPClientAddr client;
        SocketReturnCode retCode;

        // We send until we're done
        while (running) {

            // We wait for a buffer
            pendingWrites.acquire();
            code = HERMES_OK;


            while (!code) {
                
                // We retrieve the next thing to send    
                semaphore.acquire();
                code = Hermes_sendData(
                    &hermes, &remote, &command, &isResponse, &payloadSize, payload
                );                
                semaphore.release();

                if (code) {
                    // We don't have anything to send
                    continue;
                }

                // We look for the client's address in the local lookup table
                client = getClientAddr(remote);
                if (client.addr.s_addr == 0 && client.port == 0) {
                    // We don't know the client, so we discard the packet
                    // #Ratio
                    continue;
                }

                // Creating data vector
                data = std::vector(payload, payload + payloadSize);

                // We create the UDP frame 
                buffer = Hermes_serializeUDP(
                    isResponse ? HERMES_RES : HERMES_REQ,
                    command,
                    data
                );

                // We write it to the socket
                retCode = writeData(client, buffer);
                if (retCode) {
                    lastError = retCode;
                    running = false;
                    end();
                    break;
                }

            }

        }

    }

    std::vector<uint8_t> Hermes_serializeUDP (
        HermesUDPOPCode opCode,
        uint16_t command,
        std::vector<uint8_t> data
    ) {

        std::vector<uint8_t> out;

        // Allocating enough space for the header
        out.resize(HERMES_UDP_HEADER_SIZE);

        // Copying necessary vars
        memcpy(out.data(), &opCode, sizeof(HermesUDPOPCode));
        memcpy(
            out.data() + sizeof(HermesUDPOPCode), &command, sizeof(uint16_t)
        );

        // Copying data
        std::copy(data.begin(), data.end(), std::back_inserter(out));

        return out;

    }

    void Hermes_parseUDP (
        std::vector<uint8_t> buffer,
        HermesUDPOPCode *opCode,
        uint16_t *command,
        std::vector<uint8_t> &data
    ) {

        // Clearing output
        data.clear();

        // Getting the opCode and the command
        memcpy(opCode, buffer.data(), sizeof(HermesUDPOPCode));
        memcpy(
            command, buffer.data() + sizeof(HermesUDPOPCode), sizeof(uint16_t)
        );

        // Copying data
        std::copy(
            buffer.data() + HERMES_UDP_HEADER_SIZE, 
            buffer.data() + buffer.size(),
            std::back_inserter(data)
        );

    }

}

#endif