#include "Buffer.hpp"

using namespace Hermes;

Buffer::Buffer (void) {

    // Cleaning the newly-created buffer
    Hermes_clearBuffer(&buffer);
    buffer.data[0] = HERMES_T_NONE;

    // Indicates the first argument location
    arguments.push_back({ 0, HERMES_T_NONE });

    sender = 0;

}

Buffer::Buffer (HermesBuffer* source) {

    if (!source) {
        throw std::invalid_argument("Null pointer");
    }

    // Copying from source
    memcpy(&buffer, source, sizeof(HermesBuffer));

    // Parse arguments
    buffer.data[buffer.len] = HERMES_T_NONE;
    parseBufferArguments();

}

Buffer::Buffer(
    uint16_t command,
    uint8_t remote,
    bool isResponse,
    const char* signature,
    uint8_t sender
) {
    // Cleaning the newly-created buffer
    Hermes_clearBuffer(&buffer);
    buffer.data[0] = HERMES_T_NONE;

    // Indicates the first argument location
    arguments.push_back({ 0, HERMES_T_NONE });

    // Set header
    buffer.command = command;
    buffer.remote = remote;
    buffer.isResponse = isResponse;

    // Set sender
    this->sender = sender;

    // Set argument signature
    setSignature(signature);
}

Buffer::Buffer (Buffer &source): Buffer(&source.buffer) {

}

Buffer::Buffer(std::string& base64) {

    // Copy infos
    sender = Base64ToHermesBuffer(&buffer, base64.c_str());

    // Parse arguments
    buffer.data[buffer.len] = HERMES_T_NONE;
    parseBufferArguments();

}

Buffer::~Buffer (void) {
    // O_O
}

HermesBuffer* Buffer::raw () {
    return &buffer;
}

uint16_t Buffer::getCommand () {
    return buffer.command;
}

uint8_t Buffer::getRemote () {
    return buffer.remote;
}

uint8_t Buffer::getSender() {
    return sender;
}

uint16_t Buffer::getLength () {
    return buffer.len;
}

uint16_t Buffer::getPosition () {
    return buffer.pos;
}

bool Buffer::getIsResponse () {
    return buffer.isResponse;
}

std::vector<uint8_t> Buffer::getData () {
    uint8_t *start  = buffer.data,
            *end    = &buffer.data[buffer.len];

    return std::vector<uint8_t>(start, end);
}

void Buffer::setCommand (uint16_t cmd) {
    if (cmd > 4096) {
        throw std::out_of_range("Command number must be < 4096");
    }
    buffer.command = cmd;
}

void Buffer::setRemote (uint8_t remote) {
    buffer.remote = remote;
}

void Buffer::setSender(uint8_t sender) {
    this->sender = sender;
}

void Buffer::setIsResponse (bool isResponse) {
    buffer.isResponse = isResponse;
}

void Buffer::setData (std::vector<uint8_t> data) {
    
    if (data.size() >  HERMES_MAX_BUFFER_LEN) {
        throw std::length_error(
            "Invalid data size, must be < " 
            + std::to_string(HERMES_MAX_BUFFER_LEN)
        );
    }    
    
    std::copy(data.begin(), data.end(), buffer.data);
    buffer.len = data.size();
    buffer.pos = buffer.len;
    buffer.data[buffer.len] = HERMES_T_NONE;

    parseBufferArguments();

}

void Buffer::parseBufferArguments () {

    // Forget the previously known args
    arguments.clear();

    // Read the arguments
    uint8_t* typeLoc;
    uint8_t rawType;
    HermesArgumentTypeIndicator type;

    uint16_t pos;

    bool end = false;

    for (
        pos = 0; 
        pos < buffer.len && !end; 
        end = (type == HERMES_T_NONE)
    ) {

        typeLoc = &buffer.data[pos];
        std::copy(typeLoc, typeLoc + sizeof(uint8_t), &rawType);
        type = static_cast<HermesArgumentTypeIndicator>(rawType);

        arguments.push_back({ pos, type });

        // Finding jump length
        pos += sizeof(uint8_t); // Jump indicator
        pos += Buffer::argumentLength(typeLoc, buffer.len - pos);

    }

}

std::string Buffer::getSignature () {

    std::string signature = "";
    char c;

    for (auto& arg : arguments) {

        if (arg.type == HERMES_T_NONE) {
            // No indication for "none"
            break;
        }

        c = static_cast<char>(arg.type);
        signature += c;

    }

    return signature;

}

void Buffer::setSignature(std::string signature) {

    // Check if signature is valid
    for (const char& c : signature) {

        switch (c)
        {
        case 'c':
        case 'o':
        case 'x':
        case 'i':
        case 'l':
        case 'f':
        case 'd':
        case 's':
        case 'b':
        case 0:
            continue;
        
        default:
            throw std::invalid_argument(
                "Invalid type '" + std::to_string(c) + "' in signature"
            );
        }

    }

    // Clear current args
    arguments.clear();
    arguments.push_back({ 0, HERMES_T_NONE });
    buffer.data[0] = HERMES_T_NONE;

    // Add arguments
    HermesArgumentTypeIndicator type;
    for (const char& c : signature) {
        type = static_cast<HermesArgumentTypeIndicator>(c);
        addArgument(type);
    }

}

void Buffer::addArgument (HermesArgumentTypeIndicator type) {

    auto size    = arguments.size(),
         dataLen = argumentLength(type);

    if (size < 1) {
        throw std::logic_error("Arguments should never be empty");
    }

    Argument& arg = arguments[size - 1];

    if (arg.type == HERMES_T_BLOB) {
        throw std::logic_error("Can't add argument after blob");
    } else if (
        arg.type != HERMES_T_NONE ||
        buffer.len + dataLen > HERMES_MAX_BUFFER_LEN
    ) {
        throw std::length_error("Buffer data full");
    }

    arg.type = type;
    buffer.data[arg.pos] = static_cast<uint8_t>(type);
    
    // Init string as empty
    if (type == HERMES_T_STRING) {
        buffer.data[arg.pos + 1] = 0;
    }

    // Update buffer length
    buffer.len += 1 + dataLen;

    // Blob can't have a next argument
    if (type == HERMES_T_BLOB) {
        return;
    }

    // Add slot for the next argument [if not full]
    if (buffer.len >= HERMES_MAX_BUFFER_LEN) {
        return; // FULL
    } 

    arguments.push_back({ buffer.len, HERMES_T_NONE });
    buffer.data[buffer.len] = HERMES_T_NONE;

}

size_t Buffer::argumentLength (HermesArgumentTypeIndicator type) {
    switch (type)           // Jump data
    {
        case HERMES_T_CHAR:
        case HERMES_T_INT8:
            return sizeof(int8_t);
            break;
        case HERMES_T_INT16:
            return sizeof(int16_t);
            break;
        case HERMES_T_INT32:
            return sizeof(int32_t);
            break;
        case HERMES_T_INT64:
            return sizeof(int64_t);
            break;
        case HERMES_T_FLOAT:
            return sizeof(float);
            break;
        case HERMES_T_DOUBLE:
            return sizeof(double);
            break;
        case HERMES_T_STRING: {
            return 1;
            break;
        }
        case HERMES_T_BLOB: {
            return 0; // Blob goes until the end
            break;
        }

        default:
            return 0;
    }
}

size_t Buffer::argumentLength (uint8_t *argument, uint16_t maxLen) {

    uint8_t rawType;
    HermesArgumentTypeIndicator type;

    // Get type
    std::copy(argument, argument + sizeof(uint8_t), &rawType);
    type = static_cast<HermesArgumentTypeIndicator>(rawType);

    // Return size
    size_t len = argumentLength(type);
    
    if (type == HERMES_T_STRING) {
        len += strlen((char*) (argument + sizeof(uint8_t)));
    } else if (type == HERMES_T_BLOB) {
        len = maxLen;
    }

    return len;

};

Buffer::Argument::Value Buffer::get (size_t n) {

    if ((n >= arguments.size()) || arguments[n].type == HERMES_T_NONE) {
        throw std::out_of_range("Index out of range");
    }

    Argument arg = arguments[n];
    Argument::Value out;

    uint8_t *argLoc = &buffer.data[arg.pos];
    void *data = &buffer.data[arg.pos + 1];
    auto len = Buffer::argumentLength(argLoc, buffer.len - arg.pos - 1);

    switch (arg.type) {
        
    case HERMES_T_CHAR: {
        auto ptr = reinterpret_cast<char*>(data);
        std::copy(ptr, ptr+len, &out.c);
        break;
    }
    case HERMES_T_INT8: {
        auto ptr = reinterpret_cast<int8_t*>(data);
        std::copy(ptr, ptr + len, &out.i8);
        break;
    }
    case HERMES_T_INT16: {
        auto ptr = reinterpret_cast<int16_t*>(data);
        std::copy(ptr, ptr + len, &out.i16);
        break;
    }
        case HERMES_T_INT32:
            out.i32 = *reinterpret_cast<int32_t*>(data);
            break;
        case HERMES_T_INT64:
            out.i64 = *reinterpret_cast<int64_t*>(data);
            break;
        
        case HERMES_T_FLOAT:
            out.f = *reinterpret_cast<float*>(data);
            break;
        case HERMES_T_DOUBLE:
            out.d = *reinterpret_cast<double*>(data);
            break;
        
        case HERMES_T_STRING:
            out.str = reinterpret_cast<const char*>(data);
            break;
        case HERMES_T_BLOB:
            out.blob = reinterpret_cast<const uint8_t*>(data);
            break;

        default:
            throw std::logic_error("Invalid argument type");
    }

    return out;

}

HermesArgumentTypeIndicator Buffer::getType(size_t n) {
    
    if ((n >= arguments.size()) || arguments[n].type == HERMES_T_NONE) {
        throw std::out_of_range("Index out of range");
    }

    return arguments[n].type;

}

std::string Buffer::getString(size_t n) {

    if ((n >= arguments.size()) || arguments[n].type == HERMES_T_NONE) {
        throw std::out_of_range("Index out of range");
    }

    if (arguments[n].type != HERMES_T_STRING) {
        throw std::runtime_error("Argument is not a string");
    }

    return std::string(get(n).str);

}

std::vector<uint8_t> Buffer::getBlob(size_t n) {

    if ((n >= arguments.size()) || arguments[n].type == HERMES_T_NONE) {
        throw std::out_of_range("Index out of range");
    }

    if (arguments[n].type != HERMES_T_BLOB) {
        throw std::runtime_error("Argument is not a blob");
    }

    uint8_t* start = &buffer.data[arguments[n].pos];
    uint8_t* end   = &buffer.data[buffer.len];

    return std::vector(start, end);

}


void Buffer::set(size_t n, char c) {

    if ((n >= arguments.size()) || arguments[n].type == HERMES_T_NONE) {
        throw std::out_of_range("Index out of range");
    }

    if (arguments[n].type != HERMES_T_CHAR) {
        throw std::runtime_error("Argument is not a char");
    }

    std::copy(&c, &c + sizeof(char), &buffer.data[arguments[n].pos + 1]);

}

void Buffer::set(size_t n, int8_t i8) {

    if ((n >= arguments.size()) || arguments[n].type == HERMES_T_NONE) {
        throw std::out_of_range("Index out of range");
    }

    if (arguments[n].type != HERMES_T_INT8) {
        throw std::runtime_error("Argument is not a 8-bit integer");
    }

    std::copy(&i8, &i8 + sizeof(int8_t), &buffer.data[arguments[n].pos + 1]);

}

void Buffer::set(size_t n, int16_t i16) {

    if ((n >= arguments.size()) || arguments[n].type == HERMES_T_NONE) {
        throw std::out_of_range("Index out of range");
    }

    if (arguments[n].type != HERMES_T_INT16) {
        throw std::runtime_error("Argument is not a 16-bit integer");
    }

    auto ptr = reinterpret_cast<uint8_t*>(&i16);
    std::copy(ptr, ptr + sizeof(int16_t), &buffer.data[arguments[n].pos + 1]);

}

void Buffer::set(size_t n, int32_t i32) {

    if ((n >= arguments.size()) || arguments[n].type == HERMES_T_NONE) {
        throw std::out_of_range("Index out of range");
    }

    if (arguments[n].type != HERMES_T_INT32) {
        throw std::runtime_error("Argument is not a 32-bit integer");
    }

    auto ptr = reinterpret_cast<uint8_t*>(&i32);
    std::copy(ptr, ptr + sizeof(int32_t), &buffer.data[arguments[n].pos + 1]);

}

void Buffer::set(size_t n, int64_t i64) {

    if ((n >= arguments.size()) || arguments[n].type == HERMES_T_NONE) {
        throw std::out_of_range("Index out of range");
    }

    if (arguments[n].type != HERMES_T_INT64) {
        throw std::runtime_error("Argument is not a 64-bit integer");
    }

    auto ptr = reinterpret_cast<uint8_t*>(&i64);
    std::copy(ptr, ptr + sizeof(int64_t), &buffer.data[arguments[n].pos + 1]);

}

void Buffer::set(size_t n, float f) {

    if ((n >= arguments.size()) || arguments[n].type == HERMES_T_NONE) {
        throw std::out_of_range("Index out of range");
    }

    if (arguments[n].type != HERMES_T_FLOAT) {
        throw std::runtime_error("Argument is not a float");
    }

    auto ptr = reinterpret_cast<uint8_t*>(&f);
    std::copy(ptr, ptr + sizeof(float), &buffer.data[arguments[n].pos + 1]);

}

void Buffer::set(size_t n, double d) {

    if ((n >= arguments.size()) || arguments[n].type == HERMES_T_NONE) {
        throw std::out_of_range("Index out of range");
    }

    if (arguments[n].type != HERMES_T_DOUBLE) {
        throw std::runtime_error("Argument is not a double");
    }

    auto ptr = reinterpret_cast<uint8_t*>(&d);
    std::copy(ptr, ptr + sizeof(double), &buffer.data[arguments[n].pos + 1]);

}

void Buffer::set(size_t n, const char* str) {

    if ((n >= arguments.size()) || arguments[n].type == HERMES_T_NONE) {
        throw std::out_of_range("Index out of range");
    }

    Argument& currentArg = arguments[n];

    if (currentArg.type != HERMES_T_STRING) {
        throw std::runtime_error("Argument is not a string");
    }

    // Getting the current string location
    char* data = reinterpret_cast<char*>(
        &buffer.data[currentArg.pos + 1]
    );

    // Computing space requirement
    size_t currentLen = strlen(data);
    size_t futureLen = strlen(str);
    size_t bufferLen = buffer.len + futureLen - currentLen;

    if (bufferLen > HERMES_MAX_BUFFER_LEN) {
        throw std::runtime_error("Not enough space left in buffered data");
    }

    // Relocating following arguments
    int16_t deltaPos = futureLen - currentLen;
    
    // Relocate bytes
    uint16_t startPos = currentArg.pos + 2 + currentLen;
    uint16_t endPos = buffer.len;
    uint16_t byteCount = endPos - startPos;
    uint16_t targetPos = currentArg.pos + 2 + futureLen;

    // Delta < 0 => relocate to left => ascending memory
    if (deltaPos < 0) {
        for (uint16_t i = 0; i < byteCount; i++) {
            buffer.data[targetPos + i] = buffer.data[startPos + i];
        }
    }

    // Delta > 0 => relocate to right => descending memory
    else {
        for (int16_t i = byteCount - 1; i >= 0; i--) {
            buffer.data[targetPos + i] = buffer.data[startPos + i];
        }
    }

    // Update arguments positions
    for (size_t i = (n + 1); i < arguments.size(); i++) {
        arguments[i].pos += deltaPos;
    }

    // Updating buffer length
    buffer.len = bufferLen;

    // Copying the string
    std::copy(str, str + futureLen + 1, data);

}

void Buffer::set(size_t n, std::string str) {
    set(n, str.c_str());
}

void Buffer::set(size_t n, const uint8_t* blob, size_t size) {

    if ((n >= arguments.size()) || arguments[n].type == HERMES_T_NONE) {
        throw std::out_of_range("Index out of range");
    }

    Argument& currentArg = arguments[n];

    if (currentArg.type != HERMES_T_BLOB) {
        throw std::runtime_error("Argument is not a blob");
    }

    // Check if there is enough space
    size_t bufferLen = currentArg.pos + 1 + size;
    if (bufferLen > HERMES_MAX_BUFFER_LEN) {
        throw std::runtime_error("Not enough space left in buffered data");
    }

    // Update buffer length
    buffer.len = bufferLen;

    // Copy the blob
    std::copy(blob, blob + size, &buffer.data[currentArg.pos + 1]);

}

void Buffer::set(size_t n, std::vector<uint8_t> blob) {
    set(n, blob.data(), blob.size());
}


std::string Buffer::argumentType(HermesArgumentTypeIndicator type) {

    switch (type)
    {
    case HERMES_T_CHAR:
        return "char";
    case HERMES_T_INT8:
        return "int8_t";
    case HERMES_T_INT16:
        return "int16_t";
    case HERMES_T_INT32:
        return "int32_t";
    case HERMES_T_INT64:
        return "int64_t";
    case HERMES_T_FLOAT:
        return "float";
    case HERMES_T_DOUBLE:
        return "double";
    case HERMES_T_STRING:
        return "char*";
    case HERMES_T_BLOB:
        return "uint8_t*";

    default:
        return "invalid";
    }

}

std::string Buffer::argumentToString(uint16_t index) {

    if (index > arguments.size()) {
        throw std::out_of_range("Argument index out of range");
    }

    Argument& arg = arguments[index];

    switch (arg.type)
    {
    case HERMES_T_CHAR: {
        char c = get(index).c;
        char hex[3] = { 0, 0, 0 };

        if ('!' <= c && c <= '~') 
            return std::to_string(c);
        else {
            sprintf(hex, "%02x", c);
            return "0x" + std::string(hex);
        }
    }

    case HERMES_T_INT8:
        return std::to_string(get(index).i8);
    case HERMES_T_INT16:
        return std::to_string(get(index).i16);
    case HERMES_T_INT32:
        return std::to_string(get(index).i32);
    case HERMES_T_INT64:
        return std::to_string(get(index).i64);
    case HERMES_T_FLOAT:
        return std::to_string(get(index).f);
    case HERMES_T_DOUBLE:
        return std::to_string(get(index).d);
    case HERMES_T_STRING:
        return std::string(get(index).str);

    case HERMES_T_BLOB: {
        std::string blob = "[";
        char hex[5] = { 0,0,0 };

        // First element
        if (arg.pos + 1 < buffer.len) {
            sprintf(hex, "%02x", buffer.data[arg.pos + 1]);
            blob += "0x";
            blob += hex;
        }
        // All elements
        for (uint16_t i = arg.pos + 2; i < buffer.len; i++) {
            sprintf(hex, "%02x", buffer.data[i]);
            blob += ", 0x";
            blob += hex;
        }

        blob += "]";
        return blob;
    }
   
    default:
        return "invalid";
    }

}

std::string Buffer::repr() {

    const std::string crlf      ("\r\n");
    const std::string argStart  ("    - ");
    const std::string argIndent ("      ");

    std::string out = "";

    // Basic properties
    out += "sender: " + std::to_string(sender) + crlf;
    out += "remote: " + std::to_string(buffer.remote) + crlf;
    out += "command: " + std::to_string(buffer.command) + crlf;
    out += "length: " + std::to_string(buffer.len) + crlf;
    out += "is_response: " + ((buffer.isResponse ? "true" : "false") + crlf);

    // Arguments
    if (arguments.size() && arguments[0].type != HERMES_T_NONE) {
        out += /*crlf +*/ "arguments: " + crlf;
        for (auto i = 0; i < arguments.size(); i++) {
            if (arguments[i].type == HERMES_T_NONE) break;

            out += argStart + "type: " + Buffer::argumentType(arguments[i].type) + crlf;
            out += argIndent + "value: ";

            out += argumentToString(i);

            out += crlf /*+ crlf*/;

        }
    }

    return out;

}


std::string Buffer::base64() {

    char* encoded = new char[Base64SerializedBufferLen(&buffer) + 1];
    HermesBufferToBase64(encoded, &buffer, sender);

    std::string out(encoded);
    delete[] encoded;

    return out;

}