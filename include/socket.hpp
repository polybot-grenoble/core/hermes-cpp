#pragma once
#ifdef HERMES_CPP_V1

#include <stdint.h>

namespace Core::Sockets {
 
    typedef enum {
        /** No problem */
        SOCK_OK,
        /** The CAN socket can't be created */
        SOCK_SOCKET_CREATE_FAIL,
        /** The CAN socket can't be bound */
        SOCK_SOCKET_BIND_FAIL,
        /** The CAN socket is not opened */
        SOCK_SOCKET_NOT_OPENED,
        /** The payload is too big */
        SOCK_PAYLOAD_TOO_BIG,
        /** The write operation failed */
        SOCK_WRITE_FAIL,
        /** The read operation failed */
        SOCK_READ_FAIL,

    } SocketReturnCode;

}

#endif