#pragma once
#ifdef HERMES_CPP_V1

#include <thread>
#include <chrono>
#include <semaphore>

extern "C" {
    #include "Hermes.h"
}
#include "socketcan.hpp"

namespace Core::Sockets {

    /**
     * @brief A wrapper C++ class for Hermes 
     */
    class HermesCAN : public SocketCAN {

        private:

            /** The last SocketCAN return code */
            SocketReturnCode lastError = SOCK_OK;

            /** Is Hermes running */
            bool running;

            /** 
             * Internal semaphore for thread-safe operations, initialized to 1 
             */
            std::binary_semaphore semaphore{1}; 

            std::counting_semaphore<HERMES_BUFFER_COUNT> pendingWrites{0};

            /** Internal reading thread */
            std::thread reader;

            /** Internal writer thread */
            std::thread writer;


        public:
            /** The C hermes struct */
            Hermes_t hermes;

            /**
             * @brief Construct a new HermesCAN object with ID 1
             */
            HermesCAN ();

            /**
             * @brief Construct a new HermesCAN object
             * 
             * @param id The ID of this device
             */
            HermesCAN (int id);

            /**
             * @brief Destroy the HermesCAN object
             */
            ~HermesCAN ();

            /**
             * @brief Starts the HermesCAN Thread
             * 
             * @return true Hermes started
             * @return false SocketCAN is not opened
             */
            bool start ();

            /**
             * @brief Stops Hermes. Blocking until thread join
             */
            void stop ();

            /**
             * @brief Returns the last available SocketCAN error code
             * 
             * @return SocketReturnCode 
             */
            SocketReturnCode getLastError ();

            /**
             * @brief Indicates if Hermes is running 
             */
            bool isRunning ();

            /**
             * @brief Get the next recieved buffer by copying the one recieved
             * 
             * @param buffer The buffer object to copy to
             * @return true A buffer has been copied,
             * @return false No buffer copied
             */
            bool getBuffer (HermesBuffer *buffer);

            /**
             * @brief Sends a buffer on the CAN bus
             * 
             * @param buffer The buffer to send
             * @return true The buffer has been sent,
             * @return false The buffer has been lost
             */
            bool sendBuffer (HermesBuffer *buffer);
            
            /**
             * @brief Forces hermes to send all pending buffers left in its 
             * queue 
             */
            void flush ();

            /**
             * @brief Reading thread for Hermes 
             */
            void readingThread ();

            /**
             * @brief Writer thread for Hermes
             */
            void writingThread ();

    };

}

#endif