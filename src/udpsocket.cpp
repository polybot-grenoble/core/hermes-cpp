#include "udpsocket.hpp"
#ifdef HERMES_CPP_V1

using namespace Core::Sockets;
    

UDPSocket::UDPSocket () {

    // Nothing yet
    
}

UDPSocket::~UDPSocket () {

    // Nothing yet
    
}

SocketReturnCode UDPSocket::open (uint16_t port) {

    struct sockaddr_in addr;

    this->port = port;

    // Creates a socket
    socketID = socket(AF_INET, SOCK_DGRAM, 0);
    if (socketID < 0) {
        return SOCK_SOCKET_CREATE_FAIL;
    }

    // Sets-up the UDPSocket addr struct
    memset(&addr, 0, sizeof(sockaddr_in));
    addr.sin_family         = AF_INET;
    addr.sin_port           = htons(port);
    addr.sin_addr.s_addr    = htonl(INADDR_ANY);
    
    // Saving the params
    self.port = addr.sin_port;
    self.addr = addr.sin_addr;

    // Binding the socket
    if (bind(socketID, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
        return SOCK_SOCKET_BIND_FAIL;
    }

    opened = true;

    return SOCK_OK;

}

void UDPSocket::end () {

    std::vector<uint8_t> emptyness;
    writeData(self, emptyness);

    opened = false;

    close(socketID);

}

SocketReturnCode UDPSocket::writeData (
    UDPClientAddr addr, 
    std::vector<uint8_t> &data
) {

    struct sockaddr_in client;
    constexpr size_t struct_size = sizeof(sockaddr_in);

    // Not wasting time
    if (!opened) {
        return SOCK_SOCKET_NOT_OPENED;
    }

    // Not sending a packet which is too large
    if (data.size() > CORE_MAX_UDP_LEN) {
        return SOCK_PAYLOAD_TOO_BIG;
    }
    
    // Setting the client UDP address 
    memset(&client, 0, sizeof(sockaddr_in));
    client.sin_family   = AF_INET;
    client.sin_port     = addr.port;
    client.sin_addr     = addr.addr;
    
    // Sending data on the socket
    if (sendto(
            socketID, data.data(), data.size(), 
            0, (struct sockaddr *) &client, struct_size
        ) < 0) {

        return SOCK_WRITE_FAIL;

    }

    return SOCK_OK;

}

SocketReturnCode UDPSocket::readData (
    UDPClientAddr &addr, 
    std::vector<uint8_t> &data
) {

    struct sockaddr_in client;
    socklen_t struct_size = sizeof(sockaddr_in);
    size_t nBytes;
    uint8_t buffer[CORE_MAX_UDP_LEN];

    if (!opened) {
        return SOCK_SOCKET_NOT_OPENED;
    }

    nBytes = recvfrom(
        socketID, &buffer, CORE_MAX_UDP_LEN, 0, 
        (struct sockaddr *) &client, &struct_size);

    if (nBytes < 0) {
        return SOCK_READ_FAIL;
    }
   
    addr = { client.sin_addr, client.sin_port };
    data = std::vector(buffer, buffer + nBytes);

    return SOCK_OK;

}

#endif