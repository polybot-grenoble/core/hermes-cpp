#include "socketcan.hpp"
#ifdef HERMES_CPP_V1

using namespace Core::Sockets;
    

SocketCAN::SocketCAN () {

    // Nothing yet
    
}

SocketCAN::~SocketCAN () {

    // Nothing yet
    
}

SocketReturnCode SocketCAN::open (char* iface) {

    struct sockaddr_can addr;
    struct ifreq ifr;

    // Creates a socket
    socketID = socket(PF_CAN, SOCK_RAW, CAN_RAW);
    if (socketID < 0) {
        return SOCK_SOCKET_CREATE_FAIL;
    }

    // Requests Linux the interface's index
    strcpy(ifr.ifr_name, iface);
    ioctl(socketID, SIOCGIFINDEX, &ifr);

    // Sets-up the SocketCAN addr struct
    addr.can_family = AF_CAN;
    addr.can_ifindex = ifr.ifr_ifindex;

    // Binding the socket
    if (bind(socketID, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
        return SOCK_SOCKET_BIND_FAIL;
    }

    opened = true;

    return SOCK_OK;

}

SocketReturnCode SocketCAN::writeData (uint32_t id, void *data, uint32_t len) {

    struct can_frame frame;
    constexpr size_t struct_size = sizeof(struct can_frame);

    if (!opened) {
        return SOCK_SOCKET_NOT_OPENED;
    }

    if (len > CAN_MAX_DLEN) {
        return SOCK_PAYLOAD_TOO_BIG;
    }

    frame.can_id = id | (1 << 31);
    frame.can_dlc = len;
    memcpy(frame.data, data, len);

    if (write(socketID, &frame, struct_size) != struct_size) {
        return SOCK_WRITE_FAIL;
    }

    return SOCK_OK;

}


SocketReturnCode SocketCAN::readData (uint32_t *id, void *data, uint32_t *len) {

    struct can_frame frame;
    size_t nBytes;

    if (!opened) {
        return SOCK_SOCKET_NOT_OPENED;
    }

    nBytes = read(socketID, &frame, sizeof(struct can_frame));

    if (nBytes < 0) {
        return SOCK_READ_FAIL;
    }

    /* paranoid check ... */
    if (nBytes < sizeof(struct can_frame)) {
        return SOCK_READ_FAIL;
    }

    *id = frame.can_id;
    *len = frame.can_dlc;
    memcpy(data, frame.data, *len);

    return SOCK_OK;

}

#endif