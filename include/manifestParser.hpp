#pragma once

#include <string>
#include <vector>
#include <iostream>

#include "Hermes.h"

namespace Hermes {

	class PeripheralCommandArgument {
	
	private:
		// Memory location of type
		const void* typePtr = nullptr;
		// Memory location of name
		const void* namePtr = nullptr;

	public:
		/**
		 * @brief Does nothing
		 */
		PeripheralCommandArgument();

		/**
		 * @brief Creates an argument wrapper for a memory location
		 * @param mem Memory location of the argument data
		 */
		PeripheralCommandArgument(const uint8_t* mem);

		/**
		 * @brief Copies another argument
		 * @param arg The other argument
		 */
		PeripheralCommandArgument(const PeripheralCommandArgument& arg);

		/**
		 * @brief Reads the type of argument from memory
		 * @return Type of the argument 
		 */
		HermesArgumentTypeIndicator type() const;

		/**
		 * @brief Reads the name of the argument from memory.
		 * @return Name of the argument
		 */
		std::string name() const;

		/**
		 * @brief Computes the size of the argument in memory
		 * @return Length of argument data
		 */
		size_t memLength() const;

	};

	class PeripheralCommand {

	private:
		// Memory location
		const void* mem;
		// Memory location of ID
		const void* idPtr;
		// Memory location of name
		const void* namePtr;
		// Memory location of description
		const void* descriptionPtr;

	public:
		/**
		 * @brief Command input arguments
		 */
		std::vector<PeripheralCommandArgument> inputs;

		/**
		 * @brief Command output arguments
		 */
		std::vector<PeripheralCommandArgument> outputs;

		/**
		 * @brief Does nothing
		 */
		PeripheralCommand();

		/**
		 * @brief Creates an command wrapper for a memory location
		 * @param mem Memory location of the command data
		 */
		PeripheralCommand(const uint8_t* mem);

		/**
		 * @brief Copies another command
		 * @param cmd The other command
		 */
		PeripheralCommand(const PeripheralCommand& cmd);

		/**
		 * @brief Reads the command ID from memory 
		 * @return Command ID
		 */
		uint16_t id() const;

		/**
		 * @brief Reads the command name from memory
		 * @return command name
		 */
		std::string name() const;

		/**
		 * @brief Reads the command description from memory
		 * @return Command description
		 */
		std::string description() const;

		/**
		 * @brief Reads the responds flag from memory
		 * @return command responds flag
		 */
		bool responds() const;

		/**
		 * @return Length of the command in memory
		 */
		size_t memLength() const;

	};

	class PeripheralManifest {

	private:
		// Size of the manifest
		size_t memPtrLength;
		// Local copy of the manifest
		void* memPtr;

		// Memory location of Peripehral ID
		void* idPtr;
		// Memory location of IPv4 
		void* ipPtr;
		// Memory location of Port
		void* portPtr;
		// Memory location of Family
		void* familyPtr;


	public:
		/**
		 * @brief List of peripheral commands
		 */
		std::vector<PeripheralCommand> commands;

		/**
		 * @brief Initializes an empty manifest
		 */
		PeripheralManifest();

		/**
		 * @brief Loads and copies a manifest
		 * @param mem Memory location of the original manifest
		 */
		PeripheralManifest(const void* mem);

		/**
		 * @brief Copies another manifest
		 * @param manifest The other manifest
		 */
		PeripheralManifest(const PeripheralManifest& manifest);

		/**
		 * @brief Cleans up its copy of the manifest
		 */
		~PeripheralManifest();

		/**
		 * @brief Reads the peripheral ID from memory
		 * @return Peripheral ID
		 */
		uint8_t id() const;

		/**
		 * @brief Reads the peripehral IPv4 address from memory
		 * @return IPv4
		 */
		std::string ipv4() const;

		/**
		 * @brief Reads the peripheral IP Port from memory
		 * @return Peripheral IP Port
		 */
		uint16_t port() const;

		/**
		 * @brief Reads the peripheral family from memory
		 * @return peripheral family
		 */
		std::string family() const;

		/**
		 * @return Length of the manifest in memory
		 */
		size_t memLength() const;

	};


	std::ostream& operator<<(std::ostream& out, const PeripheralCommandArgument& arg);
	std::ostream& operator<<(std::ostream& out, const PeripheralCommand& cmd);
	std::ostream& operator<<(std::ostream& out, const PeripheralManifest& manifest);


}